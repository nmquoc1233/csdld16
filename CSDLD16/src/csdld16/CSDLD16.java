/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csdld16;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class CSDLD16 {

    static Connection conn;

    public static Connection getJDBC() {

        final String url = "jdbc:mysql://127.0.0.1:3307/data";
        final String user = "root";
        final String pass = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connect thanh cong.....");
            return DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException ex) {
            System.out.println("Class Bug");
        } catch (SQLException ex) {
            System.out.println("SQL Bug");
        }
        return null;
    }

    public static void main(String[] args) throws SQLException {

        conn = getJDBC();
        if (CreateTableCustomer()) {
            System.out.println("Create Customer Table Success");
        }
        if (CreateCustomer()) {
            System.out.println("Update Data To Customer Table Success");
        }

        if (CreateTableShop()) {
            System.out.println("Create Shop Table Success");
        }

        if (CreateShop()) {
            System.out.println("Update Data To Shop Table Success");
        }

        if (CreateBillTable()) {
            System.out.println("Create Bill Table Success");
        }

        if (CreateBill()) {
            System.out.println("Update Data To Bill Table Success");
        }
        if (CreateDateTable()) {
            System.out.println("Create Date Table Success");
        }

        if (CreateDate()) {
            System.out.println("Update Data To Date Table Success");
        }
    }

    public static boolean CreateTableCustomer() {
        String sql = "CREATE TABLE `data`.`customer` (\n"
                + "  `IdCustomer` INT NOT NULL AUTO_INCREMENT,\n"
                + "  `nameCustomer` VARCHAR(200) NOT NULL,\n"
                + "  `phoneCustomer` VARCHAR(45) NULL,\n"
                + "  `addressCustomer` VARCHAR(200) NULL,\n"
                + "  `typeCustomer` INT NULL,\n"
                + "  `dateOfBirth` VARCHAR(45) NULL,\n"
                + "  `genderCustomer` VARCHAR(45) NOT NULL,\n"
                + "  PRIMARY KEY (`IdCustomer`));";
        Statement statement = null;
        try {
            statement = conn.createStatement();
            statement.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CSDLD16.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    public static boolean CreateCustomer() throws SQLException {
        String sql = "INSERT INTO `data`.`customer` (`IdCustomer`, `nameCustomer`, `phoneCustomer`, `addressCustomer`, `typeCustomer`, `dateOfBirth`, `genderCustomer`) VALUES (?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 1);
        preparedStmt.setString(2, "Nguyen Ba Canh");
        preparedStmt.setString(3, "0123456789");
        preparedStmt.setString(4, "Ha Dong, Ha Noi");
        preparedStmt.setInt(5, 1);
        preparedStmt.setString(6, "12/11/1998");
        preparedStmt.setInt(7, 1);
        preparedStmt.executeUpdate();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 2);
        preparedStmt.setString(2, "Lenh Ho Xung");
        preparedStmt.setString(3, "3456789012");
        preparedStmt.setString(4, "Xung address, Ha Noi");
        preparedStmt.setInt(5, 2);
        preparedStmt.setString(6, "10/10/1658");
        preparedStmt.setInt(7, 1);
        preparedStmt.executeUpdate();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 3);
        preparedStmt.setString(2, "Trinh Tuan Cuong");
        preparedStmt.setString(3, "6789431240");
        preparedStmt.setString(4, "Dia chi 1");
        preparedStmt.setInt(5, 1);
        preparedStmt.setString(6, "23/05/1992");
        preparedStmt.setInt(7, 1);
        preparedStmt.executeUpdate();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 4);
        preparedStmt.setString(2, "Dinh Thi Quynh Trang");
        preparedStmt.setString(3, "09643357876");
        preparedStmt.setString(4, "Hai Phong");
        preparedStmt.setInt(5, 0);
        preparedStmt.setString(6, "12/06/1999");
        preparedStmt.setInt(7, 2);
        preparedStmt.executeUpdate();
        return true;

    }

    private static boolean CreateTableShop() {
        String sql = "CREATE TABLE `data`.`shop` (\n"
                + "  `shopId` INT NOT NULL AUTO_INCREMENT,\n"
                + "  `address` VARCHAR(200) NOT NULL,\n"
                + "  `phone` VARCHAR(45) NULL,\n"
                + "  `status` INT NULL COMMENT '0 - stop\\n1 - active',\n"
                + "  PRIMARY KEY (`shopId`));";
        Statement statement = null;
        try {
            statement = conn.createStatement();
            statement.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CSDLD16.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    private static boolean CreateShop() throws SQLException {

        String sql = "INSERT INTO `data`.`shop` (`shopId`, `address`, `phone`, `status`) VALUES (?, ?, ?, ?);";
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 1);
        preparedStmt.setString(2, "Nguyen Trai, Ha Dong");
        preparedStmt.setString(3, "0123");
        preparedStmt.setInt(4, 1);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 2);
        preparedStmt.setString(2, "Lang Ha, Dong Da");
        preparedStmt.setString(3, "0912321");
        preparedStmt.setInt(4, 1);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 3);
        preparedStmt.setString(2, "Quan 1, TP.HCM");
        preparedStmt.setString(3, "4321189");
        preparedStmt.setInt(4, 0);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 4);
        preparedStmt.setString(2, "Address shop, Ha Tinh");
        preparedStmt.setString(3, "012429");
        preparedStmt.setInt(4, 1);
        preparedStmt.execute();
        return true;
    }

    private static boolean CreateBillTable() {
        String sql = "CREATE TABLE `data`.`bill` (\n"
                + "  `billId` INT NOT NULL AUTO_INCREMENT,\n"
                + "  `customerId` INT NOT NULL,\n"
                + "  `shopId` INT NOT NULL,\n"
                + "  `ItemId` INT NOT NULL,\n"
                + "  `dateId` INT NOT NULL,\n"
                + "  `number` INT NULL,\n"
                + "  PRIMARY KEY (`billId`));";
        Statement statement = null;
        try {
            statement = conn.createStatement();
            statement.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CSDLD16.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private static boolean CreateBill() throws SQLException {

        String sql = "INSERT INTO `data`.`bill` (`billId`, `customerId`, `shopId`, `ItemId`, `dateId`, `number`) VALUES (?, ?, ?, ?, ?, ?);";
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 1);
        preparedStmt.setInt(2, 1);
        preparedStmt.setInt(3, 1);
        preparedStmt.setInt(4, 1);
        preparedStmt.setInt(5, 1);
        preparedStmt.setInt(6, 1);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 2);
        preparedStmt.setInt(2, 1);
        preparedStmt.setInt(3, 1);
        preparedStmt.setInt(4, 2);
        preparedStmt.setInt(5, 1);
        preparedStmt.setInt(6, 10);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 3);
        preparedStmt.setInt(2, 1);
        preparedStmt.setInt(3, 2);
        preparedStmt.setInt(4, 2);
        preparedStmt.setInt(5, 2);
        preparedStmt.setInt(6, 14);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 4);
        preparedStmt.setInt(2, 3);
        preparedStmt.setInt(3, 1);
        preparedStmt.setInt(4, 3);
        preparedStmt.setInt(5, 2);
        preparedStmt.setInt(6, 13);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 5);
        preparedStmt.setInt(2, 2);
        preparedStmt.setInt(3, 4);
        preparedStmt.setInt(4, 4);
        preparedStmt.setInt(5, 4);
        preparedStmt.setInt(6, 20);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 6);
        preparedStmt.setInt(2, 2);
        preparedStmt.setInt(3, 2);
        preparedStmt.setInt(4, 2);
        preparedStmt.setInt(5, 2);
        preparedStmt.setInt(6, 54);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 7);
        preparedStmt.setInt(2, 4);
        preparedStmt.setInt(3, 2);
        preparedStmt.setInt(4, 3);
        preparedStmt.setInt(5, 3);
        preparedStmt.setInt(6, 21);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 8);
        preparedStmt.setInt(2, 3);
        preparedStmt.setInt(3, 4);
        preparedStmt.setInt(4, 3);
        preparedStmt.setInt(5, 3);
        preparedStmt.setInt(6, 64);
        preparedStmt.execute();

        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 9);
        preparedStmt.setInt(2, 2);
        preparedStmt.setInt(3, 2);
        preparedStmt.setInt(4, 1);
        preparedStmt.setInt(5, 3);
        preparedStmt.setInt(6, 17);
        preparedStmt.execute();

        return true;
    }

    private static boolean CreateDateTable() {
        String sql = "\n"
                + "        CREATE TABLE `data`.`date` (\n"
                + "  `dateId` INT NOT NULL AUTO_INCREMENT,\n"
                + "  `time` VARCHAR(200) NOT NULL,\n"
                + "  PRIMARY KEY (`dateId`));\n"
                + "";
        Statement statement = null;
        try {
            statement = conn.createStatement();
            statement.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CSDLD16.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private static boolean CreateDate() throws SQLException {
        String sql ="INSERT INTO `data`.`date` (`dateId`, `time`) VALUES (?, ?);";
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 1);
        preparedStmt.setString(2, "06/11/2018");
        preparedStmt.execute();
        
        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 2);
        preparedStmt.setString(2, "07/11/2018");
        preparedStmt.execute();
        
        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 3);
        preparedStmt.setString(2, "08/04/2018");
        preparedStmt.execute();
        
        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 4);
        preparedStmt.setString(2, "09/04/2018");
        preparedStmt.execute();
        
        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 5);
        preparedStmt.setString(2, "10/05/2018");
        preparedStmt.execute();
        
        preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, 6);
        preparedStmt.setString(2, "20/05/2018");
        preparedStmt.execute();
        
        return false;
    }

}
