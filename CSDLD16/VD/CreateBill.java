/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Bill;

import Model.Item.Item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author HP
 */
public class CreateBill {

    public boolean createBill(Bill bill, Connection conn) throws SQLException {
        String query = "insert into bill (Time, note) values (?, ?)";
        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setString(1, bill.getTime());
        preparedStmt.setString(2, bill.getNote());
        int re = preparedStmt.executeUpdate();
        preparedStmt.close();
        return (re != 0);
    }

    public boolean addItemToBill_Item(Bill bill, Connection conn) throws SQLException {
        int re = 0;
        String sql = "insert into bill_item (bill_id, item_id, number_item) values (?, ?, ?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        for (int i = 0; i < bill.getVectorItem().size(); i++) {
            preparedStatement.setInt(1, bill.getId());
            preparedStatement.setInt(2, bill.getVectorItem().get(i).getId());
            preparedStatement.setInt(3, bill.getVectorItem().get(i).getHave());
            re = preparedStatement.executeUpdate();
        }
        preparedStatement.close();
        return (re != 0);
    }

    public boolean updateListItem(Connection conn, Bill bill) throws SQLException {
        int re = 0;
        String sql = "update item set have = have + (?) where id = (?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        for(int i=0; i<bill.getVectorItem().size(); i++){
            preparedStatement.setInt(1, bill.getVectorItem().get(i).getHave());
            preparedStatement.setInt(2, bill.getVectorItem().get(i).getId());
            re = preparedStatement.executeUpdate();
        }
        preparedStatement.close();
        return re!=0;
    }
}
